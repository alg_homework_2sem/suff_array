/*
 * #include <algorithm>
#include <iostream>
#include <vector>

using std::vector;
using std::string;

template <int alphabet>
class SuffixArray {
private:
    vector<int> sorted_array;
    vector<int> lcp;
    string str;

    void LCPCompute();
public:
    SuffixArray(const string &str);
    size_t diffSubStringsCount();
    void dump();
};

int main() {
    string s;
    std::cin >> s;
    SuffixArray<256> arr(s);
    return 0;
}

template <int alphabet>
SuffixArray<alphabet>::SuffixArray(const string &str) : str(str) {
    string s = str + '$'; // Need to choose some symbol that less than every symbol in string.
    int len = s.length();

    sorted_array.resize(len);
    vector<int> cur_count(alphabet);
    vector<int> eq_class(len);

    for (int i = 0; i < len; i++) { // This is zero phase of our algo
        cur_count[s[i]]++;
    }
    for (int i = 1; i < alphabet; i++) {
        cur_count[i] += cur_count[i - 1];
    }

    for (int i = len - 1; i >= 0; i--) {
        sorted_array[--cur_count[s[i]]] = i;
    }

    int classes_count = 1;
    eq_class[sorted_array[0]] = 0;
    for (int i = 1; i < len; i++) {
        if (s[sorted_array[i]] != s[sorted_array[i - 1]]) {
            classes_count++;
        }
        eq_class[sorted_array[i]] = classes_count - 1;
    }
    vector<int> tmp_sorted_array(len);
    vector<int> tmp_eq_class(len);
    for (int phase = 1; (1 << (phase - 1)) <= len; phase++) {
        cur_count.assign(classes_count, 0);
        for (int i = 0; i < len; i++) {
            tmp_sorted_array[i] = (sorted_array[i] - (1 << (phase - 1)) + len) % len;
        }
        for (int i = 0; i < len; i++) {
            cur_count[eq_class[tmp_sorted_array[i]]]++;
        }
        for (int i = 1; i < classes_count; i++) {
            cur_count[i] += cur_count[i - 1];
        }
        for (int i = len - 1; i >= 0; i--) {
            sorted_array[--cur_count[eq_class[tmp_sorted_array[i]]]] = tmp_sorted_array[i];
        }
        tmp_eq_class[sorted_array[0]] = 0;
        classes_count = 1;
        for (int i = 1; i < len; i++) {
            if (eq_class[sorted_array[i]] != eq_class[sorted_array[i - 1]] ||
                eq_class[(sorted_array[i] + (1 << (phase - 1))) % len] != eq_class[(sorted_array[i - 1] + (1 << (phase - 1))) % len]) {
                classes_count++;
            }
            tmp_eq_class[sorted_array[i]] = classes_count - 1;
        }
        eq_class = tmp_eq_class;
    }
    sorted_array.erase(sorted_array.begin());
}

template <int alphabet>
void SuffixArray<alphabet>::dump() {
    for (int i : sorted_array) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

template <int alphabet>
size_t SuffixArray<alphabet>::diffSubStringsCount() {
    LCPCompute();

}

template <int alphabet>
void SuffixArray<alphabet>::LCPCompute() {
    int len = str.length();
    lcp.resize(len);
    vector <int> pos(len);
    for (int i = 0; i < len; i++)
        pos[sorted_array[i]] = i;
    int k = 0;
    for (int i = 0; i < len - 1; i++) {
        if (k > 0)
            k--;
        if (pos[i] == len - 1) {
            lcp[len - 1] = -1;
            k = 0;
        } else {
            int j = sorted_array[pos[i] + 1];
            while  (std::max(i, j) + k < len && str[i + k] == str[j + k])
                k++;
            lcp[pos] = k;
        }
    }
}
*/
#include <iostream>
#include <vector>

using std::vector;
using std::string;

template <int alphabet>
class SuffixArray {
private:
    vector<int> sorted_array;
    vector<int> lcp;

    void lcpCoumpute();
public:
    SuffixArray(const string &str);
    size_t diffSubStringsCount();
    void dump();
};

int main() {
    string s;
    std::cin >> s;
    SuffixArray<256> arr(s);
    return 0;
}

template <int alphabet>
SuffixArray<alphabet>::SuffixArray(const string &str) {
    string s = str + '$'; // Need to choose some symbol that less than every symbol in string.
    int len = s.length();

    sorted_array.resize(len);
    vector<int> cur_count(alphabet);
    vector<int> eq_class(len);
    
    for (int i = 0; i < len; i++) { // This is zero phase of our algo
        cur_count[s[i]]++;
    }
    for (int i = 1; i < alphabet; i++) {
        cur_count[i] += cur_count[i - 1];
    }

    for (int i = 0; i < len; i++) {
        sorted_array[--cur_count[s[i]]] = i;
    }
        
    int classes_count = 1;
    eq_class[sorted_array[0]] = 0;
    for (int i = 1; i < len; i++) {
        if (s[sorted_array[i]] != s[sorted_array[i - 1]]) {
            classes_count++;
        }
        eq_class[sorted_array[i]] = classes_count - 1;
    }
    dump(); 
    vector<int> tmp_sorted_array(len);
    vector<int> tmp_eq_class(len);
    for (int phase = 1; (1 << phase) < len; phase++) {
        cur_count.assign(classes_count, 0);
        for (int i = 0; i < len; i++) {
           tmp_sorted_array[i] = (sorted_array[i] - (1 << phase) + len) % len;
        }
        for (int i = 0; i < len; i++) {
            cur_count[eq_class[tmp_sorted_array[i]]]++;
        }
        for (int i = 1; i < classes_count; i++) {
            cur_count[i] += cur_count[i - 1];
        }
        for (int i = 0; i < len; i++) {
            sorted_array[--cur_count[eq_class[tmp_sorted_array[i]]]] = tmp_sorted_array[i];
        }
        tmp_eq_class[sorted_array[0]] = 0;
        classes_count = 1;
        for (int i = 1; i < len; i++) {
            if (eq_class[sorted_array[i]] != eq_class[sorted_array[i - 1]] ||
                eq_class[(sorted_array[i] + (1 << phase)) % len] != eq_class[(sorted_array[i - 1] + (1 << phase)) % len]) {
                classes_count++;
            }
            tmp_eq_class[sorted_array[i]] = classes_count - 1;
        }
        dump();
        eq_class = tmp_eq_class;
    }
}

template <int alphabet>
void SuffixArray<alphabet>::dump() {
    for (int i : sorted_array) {
        std::cout << i << " ";
    }
    std::cout << std::endl; 
}
